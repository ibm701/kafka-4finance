package io.fourfinanceit.kafka.utils;


public interface Generator<T> {

    T generate();

}
