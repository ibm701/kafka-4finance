package io.fourfinanceit.kafka.utils;


import io.fourfinanceit.kafka.message.KafkaMessage;
import org.apache.commons.lang3.RandomStringUtils;

public class RandomKafkaMessageGenerator implements Generator<KafkaMessage> {

    @Override
    public KafkaMessage generate() {
        KafkaMessage kafkaMessage = new KafkaMessage();
        kafkaMessage.setApplicationId(generateApplicationId());
        kafkaMessage.setCookieId(generateCookieId());
        kafkaMessage.setUserTimestamp(generateTimestamp());
        kafkaMessage.setCustomerId(generateCustomerId());
        kafkaMessage.setUrl("https://www.vivus.es/profile");
        return kafkaMessage;
    }

    private String generateApplicationId() {
        return RandomStringUtils.randomNumeric(10);
    }

    private String generateCookieId() {
        return String.format("GA1.2.%s.%s", RandomStringUtils.randomNumeric(9), RandomStringUtils.randomNumeric(10));
    }

    private String generateTimestamp() {
        return Long.toString(System.currentTimeMillis());
    }

    private String generateCustomerId() {
        return RandomStringUtils.randomNumeric(1, 8);
    }


}
