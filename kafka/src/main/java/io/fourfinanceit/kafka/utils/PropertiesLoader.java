package io.fourfinanceit.kafka.utils;

import com.google.common.io.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private PropertiesLoader() {

    }

    public static Properties load(String resourceName) throws IOException {
        InputStream propertiesInputStream = Resources.getResource(resourceName).openStream();
        Properties properties = new Properties();
        properties.load(propertiesInputStream);
        return properties;
    }

}
