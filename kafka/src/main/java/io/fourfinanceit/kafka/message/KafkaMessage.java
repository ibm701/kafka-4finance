package io.fourfinanceit.kafka.message;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class KafkaMessage {

    String cookieId;
    String url;
    String userTimestamp;
    String customerId;
    String applicationId;

}
