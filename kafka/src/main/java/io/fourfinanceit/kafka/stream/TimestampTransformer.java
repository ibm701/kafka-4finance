package io.fourfinanceit.kafka.stream;

import io.fourfinanceit.kafka.message.KafkaMessage;
import io.fourfinanceit.kafka.message.KafkaModifiedMessage;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;


public class TimestampTransformer implements ValueTransformer<KafkaMessage, KafkaModifiedMessage> {

    private ProcessorContext context;

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public KafkaModifiedMessage transform(KafkaMessage value) {
        String timestamp = Long.toString(context.timestamp());
        KafkaModifiedMessage kafkaModifiedMessage = new KafkaModifiedMessage();
        kafkaModifiedMessage.setKafkaTimestamp(timestamp);
        kafkaModifiedMessage.setApplicationId(value.getApplicationId());
        kafkaModifiedMessage.setCookieId(value.getCookieId());
        kafkaModifiedMessage.setCustomerId(value.getCustomerId());
        kafkaModifiedMessage.setUrl(value.getUrl());
        kafkaModifiedMessage.setUserTimestamp(value.getUserTimestamp());
        return kafkaModifiedMessage;
    }

    @Override
    public KafkaModifiedMessage punctuate(long timestamp) {
        return null;
    }

    @Override
    public void close() {

    }

}
