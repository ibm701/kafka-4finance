package io.fourfinanceit.kafka;


import io.fourfinanceit.kafka.message.KafkaMessage;
import io.fourfinanceit.kafka.message.KafkaModifiedMessage;
import io.fourfinanceit.kafka.serialization.KafkaMessageSerde;
import io.fourfinanceit.kafka.serialization.KafkaModifiedMessageSerde;
import io.fourfinanceit.kafka.stream.TimestampTransformer;
import io.fourfinanceit.kafka.utils.PropertiesLoader;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.io.IOException;

public class StreamRunner {

    private final static String STREAM_PROPERTIES_FILE = "stream.properties";

    public static void main(String[] args) throws IOException {

        final Serde<String> keySerde = Serdes.String();
        final Serde<KafkaMessage> valueSerde = new KafkaMessageSerde();
        final Serde<KafkaModifiedMessage> resultSerde = new KafkaModifiedMessageSerde();

        final KStreamBuilder builder = new KStreamBuilder();
        final KStream<String, KafkaMessage> inputStream = builder.stream(keySerde, valueSerde, "kafka-rnd");
        inputStream
                .transformValues(TimestampTransformer::new)
                .to(keySerde, resultSerde, "kafka-rnd-modified");

        final KafkaStreams streams = new KafkaStreams(builder, PropertiesLoader.load(STREAM_PROPERTIES_FILE));

        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

}
