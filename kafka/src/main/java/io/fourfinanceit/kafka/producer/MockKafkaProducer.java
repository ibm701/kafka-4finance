package io.fourfinanceit.kafka.producer;

import io.fourfinanceit.kafka.message.KafkaMessage;
import io.fourfinanceit.kafka.utils.PropertiesLoader;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.io.IOException;

public class MockKafkaProducer extends KafkaProducer<String, KafkaMessage> {

    private static final String PRODUCER_PROPERTIES_FILE = "producer.properties";

    public MockKafkaProducer() throws IOException {
        super(PropertiesLoader.load(PRODUCER_PROPERTIES_FILE));
    }

}
