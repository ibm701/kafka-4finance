package io.fourfinanceit.kafka;


import io.fourfinanceit.kafka.consumer.MockKafkaModifiedConsumer;
import io.fourfinanceit.kafka.message.KafkaModifiedMessage;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.IOException;
import java.util.Collections;

public class ModifiedConsumerRunner {

    public static void main(String[] args) throws IOException {
        KafkaConsumer<String, KafkaModifiedMessage> consumer = new MockKafkaModifiedConsumer();
        consumer.subscribe(Collections.singletonList("kafka-rnd-modified"));
        while (true) {
            ConsumerRecords<String, KafkaModifiedMessage> records = consumer.poll(300);
            records.forEach((record) -> System.out.printf("offset = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value()));
        }
    }

}
