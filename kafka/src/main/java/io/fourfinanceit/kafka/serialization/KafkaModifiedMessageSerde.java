package io.fourfinanceit.kafka.serialization;

import io.fourfinanceit.kafka.message.KafkaModifiedMessage;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class KafkaModifiedMessageSerde implements Serde<KafkaModifiedMessage> {

    private final Serializer<KafkaModifiedMessage> serializer = new MessageSerializer<>();
    private final Deserializer<KafkaModifiedMessage> deserializer = new KafkaModifiedMessageDeserializer();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<KafkaModifiedMessage> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<KafkaModifiedMessage> deserializer() {
        return deserializer;
    }

}
