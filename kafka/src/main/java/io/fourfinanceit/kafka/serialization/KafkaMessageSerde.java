package io.fourfinanceit.kafka.serialization;

import io.fourfinanceit.kafka.message.KafkaMessage;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class KafkaMessageSerde implements Serde<KafkaMessage> {

    private final Serializer<KafkaMessage> serializer = new KafkaMessageSerializer();
    private final Deserializer<KafkaMessage> deserializer = new KafkaMessageDeserializer();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<KafkaMessage> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<KafkaMessage> deserializer() {
        return deserializer;
    }

}
