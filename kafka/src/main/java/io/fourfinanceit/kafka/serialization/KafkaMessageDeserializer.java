package io.fourfinanceit.kafka.serialization;

import io.fourfinanceit.kafka.message.KafkaMessage;


public class KafkaMessageDeserializer extends MessageDeserializer<KafkaMessage> {

    public KafkaMessageDeserializer() {
        super(KafkaMessage.class);
    }

}
