package io.fourfinanceit.kafka.serialization;

import io.fourfinanceit.kafka.message.KafkaModifiedMessage;


public class KafkaModifiedMessageDeserializer extends MessageDeserializer<KafkaModifiedMessage> {

    public KafkaModifiedMessageDeserializer() {
        super(KafkaModifiedMessage.class);
    }

}
