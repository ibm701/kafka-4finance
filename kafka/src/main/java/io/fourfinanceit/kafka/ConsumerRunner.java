package io.fourfinanceit.kafka;


import io.fourfinanceit.kafka.consumer.MockKafkaConsumer;
import io.fourfinanceit.kafka.message.KafkaMessage;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.IOException;
import java.util.Collections;

public class ConsumerRunner {

    public static void main(String[] args) throws IOException {
        KafkaConsumer<String, KafkaMessage> consumer = new MockKafkaConsumer();
        consumer.subscribe(Collections.singletonList("kafka-rnd"));
        while (true) {
            ConsumerRecords<String, KafkaMessage> records = consumer.poll(300);
            records.forEach((record) -> System.out.printf("offset = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value()));
        }
    }

}
