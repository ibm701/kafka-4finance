package io.fourfinanceit.kafka;


import io.fourfinanceit.kafka.message.KafkaMessage;
import io.fourfinanceit.kafka.producer.MockKafkaProducer;
import io.fourfinanceit.kafka.utils.Generator;
import io.fourfinanceit.kafka.utils.RandomKafkaMessageGenerator;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;

public class ProducerRunner {

    private static final Generator<KafkaMessage> generator = new RandomKafkaMessageGenerator();

    public static void main(String[] args) throws IOException {
        KafkaProducer<String, KafkaMessage> producer = new MockKafkaProducer();
        for (Integer i = 0; i < 10; i++) {
            KafkaMessage message = generator.generate();
            producer.send(new ProducerRecord<>("kafka-rnd", i.toString(), message));
            producer.flush();
            System.out.println("Sent msg number " + i + " message " + message.toString());
        }
        producer.close();
    }

}