package io.fourfinanceit.kafka.consumer;

import io.fourfinanceit.kafka.message.KafkaModifiedMessage;
import io.fourfinanceit.kafka.utils.PropertiesLoader;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.IOException;

public class MockKafkaModifiedConsumer extends KafkaConsumer<String, KafkaModifiedMessage> {

    private final static String CONSUMER_PROPERTIES_FILE = "modified_consumer.properties";

    public MockKafkaModifiedConsumer() throws IOException {
        super(PropertiesLoader.load(CONSUMER_PROPERTIES_FILE));
    }

}
