package io.fourfinanceit.kafka.consumer;

import io.fourfinanceit.kafka.message.KafkaMessage;
import io.fourfinanceit.kafka.utils.PropertiesLoader;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.IOException;

public class MockKafkaConsumer extends KafkaConsumer<String, KafkaMessage> {

    private final static String CONSUMER_PROPERTIES_FILE = "consumer.properties";

    public MockKafkaConsumer() throws IOException {
        super(PropertiesLoader.load(CONSUMER_PROPERTIES_FILE));
    }

}
